#contributor : Andrew Gwozdziewycz <web@apgwoz.com>
#name : ftp_login(..., ..., ...)
# key : ftp_login
# --
ftp_login(${ftp_stream}, ${username}, ${password})$0