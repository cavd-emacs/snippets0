#contributor : Andrew Gwozdziewycz <web@apgwoz.com>
#name : print_r(...)
# key: print_r
# --
print_r(${expression[, bool return]})$0